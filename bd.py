import datetime
import os
from sqlalchemy import create_engine, MetaData, Table, Integer, String, \
    Column, DateTime, column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.dialects.sqlite import DATETIME
from search_files import *
from sqlalchemy import cast
from sqlalchemy.orm import mapper
from config import BDNAME, ROOT
from pathlib import Path


dt = DATETIME(storage_format="%(year)04d-%(month)02d-%(day)02dT"
                             "%(hour)02d:%(minute)02d:%(second)02d.%(microsecond)06d")

engine = create_engine(f'sqlite:///{BDNAME}.db')
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class One_file(Base):
    __tablename__ = 'files'
    name = Column(String(255), nullable=False)
    extension = Column(String(255))
    size = Column(Integer, nullable=False)
    path = Column(String(1664), nullable=False)
    created_at = Column(dt, nullable=False, primary_key=True)
    updated_at = Column(dt)
    comment = Column(String)


def do_first_scan():
    """ запись в БД информации о всех файлах """
    # удаляем все данные с таблицы
    results = session.query(
        One_file.name,
        One_file.extension,
        One_file.size,
        One_file.path,
        cast(One_file.created_at, String)
    ).delete()

    # повторный поиск всех файлов
    files = do_search()

    for i in range(0, len(files.name)):
        c1 = One_file(
            name=files.name[i],
            extension=files.extension[i],
            size=files.size[i],
            path=files.path[i],
            created_at=files.created_at[i]
        )
        session.add(c1)
    session.commit()
    return files


def get_info_files():
    """ получение массива всех записей из БД """

    results = session.query(
        One_file.name,
        One_file.extension,
        One_file.size,
        One_file.path,
        cast(One_file.created_at, String)
    )
    message = []
    for el in results:
        mes = {
            'name': el[0],
            'extension': el[1],
            'size': el[2],
            'path': el[3],
            'created_at': el[4]
        }
        message.append(mes)
    session.commit()
    return message


def get_info_file(search_word):
    """ получение конкретной записи из БД """
    results = session.query(
        One_file.name,
        One_file.extension,
        One_file.size,
        One_file.path,
        cast(One_file.created_at, String)
    )
    mes = {}
    for el in results:
        if el[4] == search_word:
            mes = {
                'name': el[0],
                'extension': el[1],
                'size': el[2],
                'path': el[3],
                'created_at': el[4]
            }
    session.commit()
    return mes


def upload_file(file):
    """ запись в БД нового файла """

    # разделяем имя по точке
    file_full_name = file['name'].split('.')

    # имя файла без расширения
    name = ''.join(file_full_name[0:-1:])

    # расширение файла
    extension = '.' + file_full_name[-1]

    # директория с именем файла
    dir_with_file = str(Path(ROOT, file['path'])) + file['name']

    # размер файла
    size = os.path.getsize(dir_with_file)

    # дата создания файла
    created_at = datetime.datetime.fromtimestamp(os.path.getmtime(dir_with_file))

    c1 = One_file(
        name=name,
        extension=extension,
        size=size,
        path=file['path'],
        created_at=created_at
    )
    session.add(c1)
    session.commit()


def delete_file(file):
    """ удаление из бд сведений о файле """
    mes = get_info_file(file['created_at'])

    res_del=session.query(
        One_file.name,
        One_file.extension,
        One_file.size,
        One_file.path,
        cast(One_file.created_at, String)
    ).filter(
        One_file.name == mes['name'],
        One_file.extension == mes['extension'],
        One_file.size == mes['size']
                   ).delete()
    session.commit()

    # создаем полный путь к файлу и удаляем файл
    root_file = str(Path(mes['path'], mes['name']))+mes['extension']
    print(root_file)
    os.remove(root_file)
