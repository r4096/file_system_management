FROM python:3.8.0-alpine
WORKDIR /BD_FILE
ADD . /BD_FILE
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python", "main.py"]