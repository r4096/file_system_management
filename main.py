import os.path
from config import ROOT
from flask import Flask, render_template, request, jsonify, flash
from werkzeug.utils import secure_filename
import bd
from pathlib import Path
app = Flask(__name__)


# формируем корневую страницу
@app.route('/')
def index():
    files = bd.do_first_scan()
    return render_template('index.html')


# получение всех записей с БД
@app.route('/get_info_files', methods=['GET'])
def get_files():
    # GET request
    if request.method == 'GET':
        message = bd.get_info_files()
        # print('ddd')
        return jsonify(message)  # сериализация JSON


# получение конкретной записи с БД
@app.route('/get_info_file', methods=['POST'])
def get_file():
    if request.method == 'POST':
        content = request.json
        # # мы получаем дату в ISO-формате, так как она уникальная, ищем по ней
        search_word = content['Data_item']
        message = bd.get_info_file(search_word)
        return jsonify(message)


# загрузка записи в БД
@app.route('/upload_file', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return render_template('index.htnl')
        file = request.files['file']

        if file.filename == '':
            flash('нет выбранного файла')
        else:
            filename = secure_filename(file.filename)
            path = request.form.get('root_file')
            file.save(os.path.join(ROOT+path, filename))
            file = {
                'name': filename,
                'path': path
                    }
            bd.upload_file(file)

        return render_template('index.html')


# удаление записи из БД
@app.route('/delete_file', methods=['POST'])
def delete_file():
    if request.method == 'POST':

        time = request.form.get('root_file')

        file = {
            'created_at': time
                }
        bd.delete_file(file)

        return render_template('index.html')

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)

