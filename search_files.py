import datetime
import os
from config import ROOT
from pathlib import Path


class Files(object):
    def __init__(self, dict_of_files):
        self.name = dict_of_files['name']
        self.extension = dict_of_files['extension']
        self.size = dict_of_files['size']
        self.path = dict_of_files['path']
        self.created_at = dict_of_files['created_at']


def do_search():

    tree = os.walk(ROOT)

    # создаем словарь
    dict_of_files = {
        'name': [],
        'extension': [],
        'size': [],
        'path': [],
        'created_at': [],
    }
    for dir_file in tree:
        # если находимся в директории, где есть файлы
        if dir_file[-1]:
            for file in dir_file[-1]:
                # разделяем имя по точке
                file_full_name = file.split('.')

                # имя файла без расширения
                name = ''.join(file_full_name[0:-1:])

                # расширение файла
                extension = '.' + file_full_name[-1]

                # расположение файла
                path = dir_file[0]

                # директория с именем файла
                dir_with_file = str(Path(path, file))

                # размер файла
                size = os.path.getsize(dir_with_file)

                # дата создания файла
                created_at = datetime.datetime.fromtimestamp(os.path.getmtime(dir_with_file))

                # добавляем значения в словарь
                dict_of_files['name'].append(name)
                dict_of_files['extension'].append(extension)
                dict_of_files['size'].append(size)
                dict_of_files['path'].append(path)
                dict_of_files['created_at'].append(created_at)

    # наполняем объект значениями
    files = Files(dict_of_files)
    return files
